package com.fc.website.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverSetUp
{
	static WebDriver driver;
	public static WebDriver initDriver(String browserName)
	{
		if(browserName.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "D:/geckodriver.exe");
		FirefoxDriver driver=new FirefoxDriver();
		return driver;
		}
		return driver;
		
		
	}
}