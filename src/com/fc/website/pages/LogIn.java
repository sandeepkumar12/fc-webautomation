package com.fc.website.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LogIn 
{
	WebDriver driver;
	public LogIn(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	/*******************************************************************LOGIN SCREEN*****/
	@FindBy(css="span[data-reactid='.0.3.1.0.0.0.0.1.0.0']")
	public WebElement login_tab;
	
	@FindBy(id="userName")
	public WebElement username;
	@FindBy(id="password")
	public WebElement password;
	@FindBy(xpath="//div[text()='Forgot Password?']")
	public WebElement forgot_password;
	
	@FindBy(xpath="//span[text()='Stay signed in']")
	
	public WebElement stay_signed_in;
	@FindBy(xpath="//span[text()='Login with Facebook']")
	public WebElement facebook_login;
	@FindBy(xpath="//span[text()='Login with Google']")
	public WebElement gmail_login;
	@FindBy(xpath="//button[contains(text(),'Login')]")
	public WebElement login_button;
	/***************************************************************/
	/*******************************************************************REGISTER SCREEN*****/
	@FindBy(css="span[data-reactid='.0.3.1.0.0.0.0.1.0.1']")
	public WebElement register_tab;
	@FindBy(id="name")
	public WebElement name_register;
	@FindBy(id="userName")
	public WebElement username_register;
	@FindBy(id="mobileNumer")
	public WebElement mobileno_register;
	@FindBy(id="password")
	public WebElement password_register;
	@FindBy(xpath="//button[text()='Signup']")
	public WebElement signup;
	@FindBy(xpath="//span[text()='Login with Facebook']")
	public WebElement facebook_login_register;
	@FindBy(xpath="//span[text()='Login with Google']")
	public WebElement gmail_login_register;
	/***************************************************************/
}
