package com.fc.website.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.fc.website.pages.LogIn;
import com.fc.website.utility.Constants;
import com.fc.website.utility.DriverSetUp;
import com.fc.website.utility.URL;



import com.fc.website.pages.HomePage;

public class LogInTest 
{
	WebDriver driver=DriverSetUp.initDriver("firefox");
	LogIn login=new LogIn(driver);
	HomePage homepage=new HomePage(driver);
	SoftAssert softassert=new SoftAssert();
	@BeforeClass
	public void setup()
	{
		 driver.get(URL.homePage_url);
	}
	@Test
	public void login_screen_verifcaition()
	{
		homepage.login_button.click();
		softassert.assertEquals(driver.getCurrentUrl(), "https://www.freecharge.in/desktop/login");
		softassert.assertEquals( login.login_tab.isDisplayed(),true);
		softassert.assertEquals( login.register_tab.isDisplayed(),true);
		//ensure "login" tab is highlighted by defaults
		softassert.assertEquals( login.login_tab.getCssValue("opacity"),"1");
		softassert.assertEquals( login.register_tab.getCssValue("opacity"),"0.6");
		softassert.assertEquals(login.forgot_password.isDisplayed(),true);
		softassert.assertEquals(login.stay_signed_in.isDisplayed(),true);
		softassert.assertEquals(login.username.isDisplayed(),true);
		softassert.assertEquals(login.password.isDisplayed(),true);
		softassert.assertEquals(login.username.getAttribute("placeholder"),"Email Id/Mobile Number");
		softassert.assertEquals(login.password.getAttribute("placeholder"),"Password");
		softassert.assertEquals(login.login_button.isDisplayed(), true);
	
	softassert.assertAll();
	}
	
	@Test(dependsOnMethods="login_screen_verifcaition")
	public void UserLogin()
	{
		login.username.sendKeys(Constants.M_username);
		login.password.sendKeys(Constants.password);
		login.login_button.click();
	}
	
	@Test(enabled=false)
	public void register_screen_verification()
	{
		login.register_tab.click();
		softassert.assertEquals(login.name_register.isDisplayed(), true,"name field is not displayed");
	//	softassert.assertEquals(login.username_register.isDisplayed(), true,"username field is not displayed");
		
		softassert.assertEquals(login.mobileno_register.isDisplayed(), true,"mobile no field is not displayed");
		//softassert.assertEquals(login.password_register.isDisplayed(), true,"password field is not displayed");
		softassert.assertEquals(login.signup.isDisplayed(), true,"signup button is displayed");
		//softassert.assertEquals(login.facebook_login_register.isDisplayed(), true,"facebook register option is present");
		//softassert.assertEquals(login.gmail_login_register.isDisplayed(), true,"gmail register option is present");
		softassert.assertAll(); 
	}
	
	@Test(enabled=true)
	public void login_with_invalidCredentials()
	{
		login.username.sendKeys("testingfc2@gmail.com");
		login.password.sendKeys("test123");
		
	}
	
	
	
	
	@AfterClass
	public void teardown()
	{
		 driver.quit();
	}

}
