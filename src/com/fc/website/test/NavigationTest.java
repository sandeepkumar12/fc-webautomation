package com.fc.website.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fc.website.pages.Navigation;
import com.fc.website.utility.DriverSetUp;
import com.fc.website.utility.URL;

public class NavigationTest 
{
 WebDriver driver=DriverSetUp.initDriver("firefox");
 Navigation navObj=new Navigation(driver);
 @BeforeClass
 public void setup()
 {
	 driver.get(URL.homePage_url);
 }
 
 @Test
 public void show()
 {
	// driver.get("https://www.freecharge.in/");
	 navObj.showNav();;
 }
 @AfterClass
 public void teardown()
 {
	 driver.quit();
 }
 
}
