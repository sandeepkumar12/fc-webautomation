package com.fc.website.test;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.fc.website.pages.HomePage;
import com.fc.website.utility.DriverSetUp;
import com.fc.website.utility.URL;



public class HomePageTest 
{


WebDriver driver=DriverSetUp.initDriver("firefox");
HomePage homepage=new HomePage(driver);
@BeforeClass
public void setup()
{
	 driver.get(URL.homePage_url);
}
@Test
public void login_button_check()
{
	driver.get(URL.homePage_url);
	Assert.assertEquals(homepage.login_button_presence(), true);
}

@AfterClass
public void teardown()
{
	 driver.quit();
}


}
